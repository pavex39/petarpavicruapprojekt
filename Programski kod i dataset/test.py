import streamlit as st
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import accuracy_score, recall_score, precision_score, confusion_matrix

class LogReg:
    def __init__(self):
        self.theta = None
        self.loss = None

    def sigmoid(self, X): #Racuna vrijednost sigmoid funkcije
        return 1 / (1 + np.exp(-X))

    def h(self, X): #Racuna vrijednost model funkcije za 
        return self.sigmoid(self.theta[0] + np.dot(X, self.theta[1:]))        #sigmoid
        #return self.theta[0] + np.dot(X, self.theta[1:])                     #identiteta
        #return np.maximum(0, self.theta[0] + np.dot(X, self.theta[1:]))      #relu
    
    def J(self, X, y): #Racuna vrijednost funkcije J za
        #X.shape = (m, n)
        #y.shape = (m, 1)
    
        m = X.shape[0]
        output = self.h(X)
        return -(1.0 / m) * np.sum(y * np.log(output) + (1 - y) * np.log(1 - output))

    def predict(self, X):
        pred = self.h(X)
        return np.array([1 if p >= 0.5 else 0 for p in pred])

    def gradient(self, X, y): #Racuna gradijent funkcije J za
        #theta.shape = (n+1, 1)
    
        m = X.shape[0]
        X_t = np.concatenate((np.ones((m, 1)), X), axis=1).T
        return (1.0 / m) * (X_t @ (self.h(X) - y))

    def fit(self, X, y, lr=0.02, num_iter=10000):
        log_interval = num_iter // 5
        m, n = X.shape
        self.theta = np.zeros((n + 1, 1))
        self.loss = np.empty(num_iter)
        for it in range(num_iter):
            self.loss[it] = self.J(X, y)
            grad = self.gradient(X, y)
            self.theta = np.subtract(self.theta, lr * grad)

with open('heart.csv') as f:
    df_heart = pd.read_csv(f)
    data = pd.DataFrame.to_numpy(df_heart)
    y = data[:, -1].reshape(-1, 1) # zadnji stupac
    X = data[:, :-1] # svi stupci osim zadnjeg
    
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)
scaler = MinMaxScaler().fit(X_train)
X_train = scaler.transform(X_train)
X_test = scaler.transform(X_test)

model = LogReg()
model.fit(X_train, y_train)


param1 =  st.number_input("The person's age in years: ",min_value=0,max_value=150,step=1)
param2 =  st.number_input("The person's sex (1 = male, 0 = female): ",min_value=0,max_value=1,step=1)
param3 =  st.number_input("The chest pain experienced (Value 1: typical angina, Value 2: atypical angina, Value 3: non-anginal pain, Value 4: asymptomatic): ",min_value=0,max_value=4,step=1)
param4 =  st.number_input("The person's resting blood pressure (mm Hg on admission to the hospital): ",min_value=0,max_value=250,step=1)
param5 =  st.number_input("The person's cholesterol measurement in mg/dl: ",min_value=0,max_value=400,step=1)
param6 =  st.number_input("The person's fasting blood sugar (> 120 mg/dl, 1 = true; 0 = false): ",min_value=0,max_value=1,step=1)
param7 =  st.number_input("Resting electrocardiographic measurement (0 = normal, 1 = having ST-T wave abnormality, 2 = showing probable or definite left ventricular hypertrophy by Estes' criteria): ",min_value=0,max_value=2,step=1)
param8 =  st.number_input("The person's maximum heart rate achieved: ",min_value=0,max_value=250,step=1)
param9 =  st.number_input("Exercise induced angina (1 = yes; 0 = no): ",min_value=0,max_value=1,step=1)
param10 =  st.number_input("ST depression induced by exercise relative to rest: ",min_value=0.0,max_value=3.0)
param11 =  st.number_input("The slope of the peak exercise ST segment (Value 1: upsloping, Value 2: flat, Value 3: downsloping): ",min_value=0,max_value=3,step=1)
param12 =  st.number_input("The number of major vessels (0-3): ",min_value=0,max_value=3,step=1)
param13 =  st.number_input("A blood disorder called thalassemia (3 = normal; 6 = fixed defect; 7 = reversable defect): ",min_value=0,max_value=7,step=1)
pacijent = np.array([param1,param2,param3,param4,param5,param6,param7,param8,param9,param10,param11,param12,param13]).reshape(1,-1)
pacijent = scaler.transform(pacijent)

testpred = model.predict(pacijent)
postotak = model.h(pacijent)[0][0]*100
if param1 !=0:
    if testpred[0] == 1:
        st.write('Sick')
    if testpred[0] == 0:
        st.write('Healthy')
    st.write("Percentage that the patient has heart disease : %.2f" % postotak,"%")

